//
// Virtual machine
//
var Op = Object.freeze({
	"NoOp": 				1,
	"Wait": 				2,
	"JumpNotZero": 			3,
	"PenDown": 				4,
	"PenUp": 				5,
	"SetPenPosition": 		6,
	"SetColour3": 			7,
	"SetColour4": 			8,
	"SetWidth": 			9,
	"MoveForward": 			10,
	"MoveBackward": 		11,
	"MoveForwardSetWidth": 	12,
	"MoveBackwardSetWidth": 13,
	"TurnLeft": 			14,
	"TurnRight": 			15,
	"Push":					16,
	"Pop":					17,
	"SetJump":				18,
	"SetLineCap":			19,
	"Suspend":				20,
	"Signal":				21,
	"Fill":					22,
	"Arc":					23
})
	
var ThreadState = Object.freeze({
	"Unknown": 				1,
	"Running": 				2,
	"Moving": 				3,
	"Suspended": 			4,
	"Finished": 			5
})

var LineCap = Object.freeze({
	"Butt":					1,
	"Round":				2,
	"Square":				3
})

function toRadians(degrees) {
	return degrees * Math.PI / 180
}

class Thread {
	
	constructor(machine, id, context, bytecode) {
		
		this.id = id
		
		// VM vars
		this.machine = machine
		this.bytecode = bytecode
		this.stack = []
		this.state = ThreadState.Running
		this.instructionPtr = 0
		this.waitTime = 0
		this.jumpTarget = 0
		this.jumpCount = 0
		this.blocks = {}
		
		// Render vars
		this.context = context
		this.penDown = true
		this.moveTime = 0
		this.startX = 0
		this.startY = 0
		this.penX = 0
		this.penY = 0
		this.dirX = 0
		this.dirY = 1
		this.targetX = 0
		this.targetY = 0
		this.speed = 0
		this.penWidth = 1
		this.startWidth = 1
		this.targetWidth = 1
		this.penColour = 'black'
		this.capType = 'round'
	}
	
	fromRGB(r, g, b) {
		return ['rgb(', r, ',', g, ',', b, ')'].join('');
	}	

	fromRGBA(r, g, b, a) {
		return ['rgba(', r, ',', g, ',', b, ',', (a / 255), ')'].join('');
	}	
	
	setTarget(distance, speed, width) {

		this.moveTime = 0
		this.startX = this.penX
		this.startY = this.penY
		this.startWidth = this.penWidth
		
		this.targetX = this.penX + this.dirX * distance
		this.targetY = this.penY + this.dirY * distance
		this.targetWidth = width;
		this.speed = speed
	}
	
	turn(angle) {
		
		var sinAngle = Math.sin(toRadians(angle))
		var cosAngle = Math.cos(toRadians(angle))

		var newX = this.dirX * cosAngle - this.dirY * sinAngle
		var newY = this.dirY * cosAngle + this.dirX * sinAngle

		this.dirX = newX
		this.dirY = newY	
	}
	
	doDraw(dx, dy, dwidth) {
		
		// For variable width lines, do the following
		var x0 = this.penX
		var y0 = this.penY
		var x1 = x0 + dx
		var y1 = y0 + dy
				
		var numSegs = 100 // try and minimise numsegs
		for (var i = 0; i < numSegs; i++) {

			var t = i / (numSegs - 1)
			var d0 = i / numSegs
			var d1 = (i + 1) / numSegs
			var dd = (d0 + d1) / 2

			this.context.beginPath()
			this.context.lineWidth = this.penWidth + dwidth * t
			this.context.lineCap = 'square'

			this.context.moveTo(x0 + (x1 - x0) * d0, y0 + (y1 - y0) * d0)
			this.context.quadraticCurveTo(x0 + (x1 - x0) * dd, y0 + (y1 - y0) * dd, x0 + (x1 - x0) * d1, y0 + (y1 - y0) * d1)

			this.context.stroke()
		}	
		
		// For fixed width, do the following
		/*
		context.beginPath()
		context.lineWidth = this.lineWidth
		context.moveTo(100, 100)
		for (var i = 0; i < 155; i++) {

			var d = i / 154

		  //context.fillStyle = 'black'
		  //context.lineJoin = 'round'
		  

		  context.lineTo(100 + 300 * d, 100 + 200 * d)
		}
		context.stroke()
		*/
	}
	
	doMove(frameTimeSecs) {

		this.moveTime += frameTimeSecs
		
		// Calculate how far to move in this update, and draw line made of circles
		var frameMoveX = this.dirX * this.speed * frameTimeSecs
		var frameMoveY = this.dirY * this.speed * frameTimeSecs
		var frameMove = Math.sqrt(frameMoveX * frameMoveX + frameMoveY * frameMoveY)
		
		var frameTargetX = this.startX + frameMoveX
		var frameTargetY = this.startY + frameMoveY
		
		var distToTargetX = this.targetX - this.startX
		var distToTargetY = this.targetY - this.startY
		var distToTarget = Math.sqrt(distToTargetX * distToTargetX + distToTargetY * distToTargetY)
		
		if (frameMove > distToTarget) {
			frameMoveX = distToTargetX
			frameMoveY = distToTargetY
			frameMove = distToTarget

			frameTargetX = this.startX + frameMoveX
			frameTargetY = this.startY + frameMoveY
		}
		
		var widthDelta = (this.targetWidth - this.startWidth) * (frameMove / distToTarget)

		if (this.penDown) {
			this.doDraw(frameMoveX, frameMoveY, widthDelta)
		}

		//console.log(frameMoveX, frameMoveY)
		this.penX += frameMoveX
		this.penY += frameMoveY
		this.penWidth += widthDelta
		
		// If we've reached the target, then continue processing bytecode
		var distToStartX = this.penX - this.startX
		var distToStartY = this.penY - this.startY
		var distToStart = Math.sqrt(distToStartX * distToStartX + distToStartY * distToStartY)
		
		if (distToStart >= distToTarget) {
			this.penX = this.targetX
			this.penY = this.targetY
			this.penWidth = this.targetWidth
			this.state = ThreadState.Running
		}
	}
	
	arc(x, y, radius, startAngle, endAngle) {
		
		this.context.beginPath()
		this.context.arc(x, y, radius, toRadians(startAngle), toRadians(endAngle))
		this.context.stroke()
	}
	
	signal(sourceId, values) {

		// Check specific blocks for sourceId
		if (sourceId in this.blocks) {
			
			let blocks = this.blocks[sourceId]
			
			values.forEach(value => {
				if (value in blocks) {
					blocks[value]--
					
					if (blocks[value] < 1) {
						delete blocks[value]
					}
				}
			})
			
			if (Object.keys(blocks).length == 0) {
				delete this.blocks[sourceId]
			}
		}
		
		// Check general blocks
		if (null in this.blocks) {
			let blocks = this.blocks[null]
			
			values.forEach(value => {
				if (value in blocks) {
					blocks[value]--
					
					if (blocks[value] < 1) {
						delete blocks[value]
					}
				}
			})
			
			if (Object.keys(blocks).length == 0) {
				delete this.blocks[null]
			}
		}
	}

	testPixel(imageData, x, y, width, red, green, blue) {
		var offset = (y * width + x) * 4
		return red == imageData.data[offset + 0] && green == imageData.data[offset + 1] && blue == imageData.data[offset + 2]
	}
	
	writePixel(imageData, x, y, width, red, green, blue) {
		var offset = (y * width + x) * 4
		imageData.data[offset + 0] = red
		imageData.data[offset + 1] = green
		imageData.data[offset + 2] = blue
	}
	
	fill(x, y, red, green, blue) {

		var canvasWidth = this.context.canvas.clientWidth
		var canvasHeight = this.context.canvas.clientHeight
		var imageData = this.context.getImageData(0, 0, canvasWidth, canvasHeight)
		
		// Get start colour
		var startColour = this.context.getImageData(x, y, 1, 1).data
		
		if (!this.testPixel(imageData, x, y, red, green, blue)) {
			this._fill(imageData, x, y, canvasWidth, canvasHeight, red, green, blue, startColour[0], startColour[1], startColour[2])
		}
		
		this.context.putImageData(imageData, 0, 0)
	}
	
	_fill(imageData, x, y, width, height, red, green, blue, rStart, gStart, bStart) {
		
		while (true) {
			let ox = x
			let oy = y
			
			while (y != 0 && this.testPixel(imageData, x, y - 1, width, rStart, gStart, bStart)) {
				y--
			}
			while (x != 0 && this.testPixel(imageData, x - 1, y, width, rStart, gStart, bStart)) {
				x--
			}

			if (x == ox && y == oy) {
				break
			}
		}
		
		this._fillcore(imageData, x, y, width, height, red, green, blue, rStart, gStart, bStart)
	}
	
	_fillcore(imageData, x, y, width, height, red, green, blue, rStart, gStart, bStart) {
		
		var lastRowLength = 0
		do {
			let rowLength = 0, sx = x
			
			if (lastRowLength != 0 && !this.testPixel(imageData, x, y, width, rStart, gStart, bStart)) {
				do {
					if (--lastRowLength == 0) {
						return
					}
				} while(!this.testPixel(imageData, ++x, y, width, rStart, gStart, bStart));
				sx = x
			} else {
				for (; x != 0 && this.testPixel(imageData, x - 1, y, width, rStart, gStart, bStart); rowLength++, lastRowLength++) {
					this.writePixel(imageData, --x, y, width, red, green, blue)
					if (y != 0 && this.testPixel(imageData, x, y - 1, width, rStart, gStart, bStart)) {
						this._fill(imageData, x, y - 1, width, height, red, green, blue, rStart, gStart, bStart)
					}
				}
			}

			for (; sx < width && this.testPixel(imageData, sx, y, width, rStart, gStart, bStart); rowLength++, sx++) {
				this.writePixel(imageData, sx, y, width, red, green, blue)
			}
			
			if (rowLength < lastRowLength) {
				for (let end = x + lastRowLength; ++sx < end; ) {
					if (this.testPixel(imageData, sx, y, width, rStart, gStart, bStart)) {
						this._fillcore(imageData, sx, y, width, height, red, green, blue, rStart, gStart, bStart)
					}
				}
			} else if (rowLength > lastRowLength && y != 0) {
				for (let ux = x + lastRowLength; ++ux < sx;) {
					if (this.testPixel(imageData, ux, y - 1, width, rStart, gStart, bStart)) {
						this._fill(imageData, ux, y - 1, width, height, red, green, blue, rStart, gStart, bStart)
					}
				}
			}	

			lastRowLength = rowLength					
		} while (lastRowLength != 0 && ++y < height)
	}
	
	update(frameTimeMs) {
		
		var frameTimeSecs = frameTimeMs / 1000
		
		// If we're moving, then do that, otherwise check to see if we're suspended
		if (this.state == ThreadState.Moving) {
			this.doMove(frameTimeSecs)
		} else if (this.state == ThreadState.Suspended) {
			
			// Check blocks
			let numBlocks = Object.keys(this.blocks).length
			let blocked = numBlocks > 0
						
			// Check wait time
			let waiting = false
			if (this.waitTime > 0) {
			
				this.waitTime -= frameTimeSecs
				if (this.waitTime > 0) {
					waiting = true
				}
			}
			
			if (blocked || waiting) {
				return
			}
			
			this.state = ThreadState.Running
		}
		
		// Now see if we're in running state
		if (this.state == ThreadState.Running) {
			while (this.instructionPtr < this.bytecode.length) {
			
				let instruction = this.bytecode[this.instructionPtr]
				this.instructionPtr++;
				
				switch (instruction) {
					
					// No instruction.
					case Op.NoOp:
						break
						
					// Suspend thread.
					// Arg 1: time to suspend, in seconds.
					case Op.Wait:
						this.waitTime = this.bytecode[this.instructionPtr]
						this.instructionPtr += 1
						this.state = ThreadState.Suspended
						return
						
					// Jump to offset in bytecode if arg 0 greater than zero.
					// Arg 1: number of times to jump.  This is modified each time.
					// Arg 2: offset to jump to.
					case Op.JumpNotZero:
						if (this.jumpCount > 0) {
							this.jumpCount--
							this.instructionPtr = this.jumpTarget
						}
						break
						
					// Set drawing to true.  Any move opcode will cause something to be drawn.
					case Op.PenDown:
						this.penDown = true
						break
					
					// Set drawing to false.  Any move opcode will not cause anything to be drawn.
					case Op.PenUp:
						this.penDown = false
						break
						
					// Teleport pen position.
					// Arg 1: new x position.
					// Arg 2: new y position.
					case Op.SetPenPosition:
						this.penX = this.bytecode[this.instructionPtr + 0]
						this.penY = this.bytecode[this.instructionPtr + 1]
						this.instructionPtr += 2;
						break
						
					// Set pen colour.  
					// Arg 1: red component (0-255).
					// Arg 2: green component (0-255).
					// Arg 3: blue component (0-255).
					case Op.SetColour3:
						this.penColour = this.fromRGB(this.bytecode[this.instructionPtr + 0], this.bytecode[this.instructionPtr + 1], this.bytecode[this.instructionPtr + 2])
						this.instructionPtr += 3
						break
						
					// Set pen colour.  
					// Arg 1: red component (0-255).
					// Arg 2: green component (0-255).
					// Arg 3: blue component (0-255).
					// Arg 3: alpha component (0-255).
					case Op.SetColour4:
						this.penColour = this.fromRGBA(this.bytecode[this.instructionPtr + 0], this.bytecode[this.instructionPtr + 1], this.bytecode[this.instructionPtr + 2], this.bytecode[this.instructionPtr + 3])
						this.instructionPtr += 4
						break

						// Set pen width.
					// Arg 1: width in pixels.
					case Op.SetWidth:
						this.penWidth = this.bytecode[this.instructionPtr + 0]
						this.instructionPtr += 1
						break;
						
					// Move the pen forward a given amount over a given time.
					// Arg 1: distance to move in pixels.
					// Arg 2: the time taken to move, in seconds.
					case Op.MoveForward:
						this.setTarget(this.bytecode[this.instructionPtr], this.bytecode[this.instructionPtr + 1], this.penWidth)
						this.instructionPtr += 2
						this.state = ThreadState.Moving
						return
						
					// Move the pen backward a given amount over a given time.
					// Arg 1: distance to move in pixels.
					// Arg 2: the time taken to move, in seconds.
					case Op.MoveBackward:
						this.setTarget(-this.bytecode[this.instructionPtr], this.bytecode[this.instructionPtr + 1], this.penWidth)
						this.instructionPtr += 2
						this.state = ThreadState.Moving
						return

					// Move the pen forward a given amount over a given time, with a target finishing width.
					// Arg 1: distance to move in pixels.
					// Arg 2: width to finish on.
					// Arg 3: the time taken to move, in seconds.
					case Op.MoveForwardSetWidth:
						this.setTarget(this.bytecode[this.instructionPtr], this.bytecode[this.instructionPtr + 2], this.bytecode[this.instructionPtr + 1])
						this.instructionPtr += 3
						this.state = ThreadState.Moving
						return
						
					// Move the pen backward a given amount over a given time, with a target finishing width.
					// Arg 1: distance to move in pixels.
					// Arg 2: width to finish on.
					// Arg 3: the time taken to move, in seconds.
					case Op.MoveBackwardSetWidth:
						this.setTarget(-this.bytecode[this.instructionPtr], this.bytecode[this.instructionPtr + 2], this.bytecode[this.instructionPtr + 1])
						this.instructionPtr += 3
						this.state = ThreadState.Moving
						return

						// Rotate the pen direction left.
					// Arg 1: angle (in degrees) to rotate left.
					case Op.TurnLeft:
						this.turn(-this.bytecode[this.instructionPtr])
						this.instructionPtr += 1
						break
						
					// Rotate the pen direction right.
					// Arg 1: angle (in degrees) to rotate right.
					case Op.TurnRight:
						this.turn(this.bytecode[this.instructionPtr])
						this.instructionPtr += 1
						break
						
					case Op.Push:
						this.stack.push(this.bytecode[this.instructionPtr])
						this.instructionPtr += 1
						break
						
					case Op.Pop:
						this.stack.pop()
						break
						
					// Set the jump target to the instruction after this one
					// Arg 1: the number of times to jump
					case Op.SetJump:
						this.jumpTarget = this.instructionPtr + 1
						this.jumpCount = this.bytecode[this.instructionPtr]
						this.instructionPtr += 1
						break
						
					// Set linecap type
					// Arg 1: type [LineCap.Butt, LineCap.Round, LineCap.Square]
					case Op.SetLineCap:
						switch (this.bytecode[this.instructionPtr]) {
							case LineCap.Butt:
								this.capType = 'butt'
								break
							
							case LineCap.Round:
								this.capType = 'round'
								break
								
							case LineCap.Square:
								this.capType = 'square'
								break
								
							default:
								throw 'Invalid linecap type'
						}
						
						this.instructionPtr += 1
						break
						
					// Suspend thread, and wait for signals
					// Arg 1: number of id/value pairs
					// Arg 2..n: id/value pairs
					case Op.Suspend:
						let numPairs = this.bytecode[this.instructionPtr]
						this.instructionPtr++
						
						let pairs = {}
						for (let i = 0; i < numPairs; i++) {
							pairs[this.bytecode[this.instructionPtr]] = this.bytecode[this.instructionPtr + 1]
							this.instructionPtr += 2
						}
						
						// Go through blocks, and add in pairs
						Object.entries(pairs).forEach(entry => {
							let id = entry[0]
							let value = entry[1]
							
							if (id in this.blocks) {
								let block = this.blocks[id]
								if (value in block) {
									block[value]++
								} else {
									block[value] = 1
								}
							} else {
								this.blocks[id] = { [value]: 1 }
							}
						})
						
						this.state = ThreadState.Suspended
						return
						
					// Signal: raise a number of values to either specific threads, or all threads
					// Arg 1: number of values
					// Arg 2..n: values
					// Arg n+1: number of threads, or zero for all
					// Arg n+2..p: thread ids
					case Op.Signal:
						let numValues = this.bytecode[this.instructionPtr]
						this.instructionPtr++
						
						let values = []
						for (let i = 0; i < numValues; i++) {
							values.push(this.bytecode[this.instructionPtr])
							this.instructionPtr++
						}
												
						let numThreads = this.bytecode[this.instructionPtr]
						this.instructionPtr++
						
						let threadIds = []
						for (let i = 0; i < numThreads; i++) {
							threadIds.push(this.bytecode[this.instructionPtr])
							this.instructionPtr++
						}
												
						this.machine.signal(this.id, threadIds, values)
						break
						
					// Flood fill a region
					// Arg 1: x position
					// Arg 2: y position
					// Arg 3: red component
					// Arg 4: green component
					// Arg 5: blue component
					case Op.Fill:
						var x = this.bytecode[this.instructionPtr + 0]
						var y = this.bytecode[this.instructionPtr + 1]
						var r = this.bytecode[this.instructionPtr + 2]
						var g = this.bytecode[this.instructionPtr + 3]
						var b = this.bytecode[this.instructionPtr + 4]
						
						this.fill(x, y, r, g, b)
						this.instructionPtr += 5
						break
						
					// Draw an arc
					// Arg 1: x position
					// Arg 2: y position
					// Arg 3: radius
					// Arg 4: start angle
					// Arg 5: end angle
					case Op.Arc:
						var x = this.bytecode[this.instructionPtr + 0]
						var y = this.bytecode[this.instructionPtr + 1]
						var radius = this.bytecode[this.instructionPtr + 2]
						var startAngle = this.bytecode[this.instructionPtr + 3]
						var endAngle = this.bytecode[this.instructionPtr + 4]
						
						this.arc(x, y, radius, startAngle, endAngle)
						this.instructionPtr += 5
						break
					
					// Unknown opcode
					default:
						throw 'Invalid opcode'
				}
			}
			
			this.state = ThreadState.Finished
		}
	}
}

class Machine {
	
	constructor(context) {
		this.threads = {}
		this.context = context
	}
	
	addThread(id, bytecode) {
		var th = new Thread(this, id, this.context, bytecode)
		this.threads[id] = th
		return th
	}
	
	signal(sourceId, targetIds, values) {
		
		if (targetIds.length == 0) {
			// Signal all threads
			Object.entries(this.threads).forEach(entry => {
				let id = entry[0]
				let thread = entry[1]
				
				if (id != sourceId) {
					thread.signal(sourceId, values)
				}
			})
		} else {
			// Signal specified threads
			targetIds.forEach(threadId => {
				if (threadId in this.threads) {
					this.threads[threadId].signal(sourceId, values)
				}
			})
		}
	}
	
	update(frameTimeMs) {
		
		var finishedThreads = []
		Object.values(this.threads).forEach(thread => {
			thread.update(frameTimeMs)
			
			if (thread.state == ThreadState.Finished) {
				finishedThreads.push(thread.id)
			}
		})
		
		// Remove all finished threads
		for (var i = 0; i < finishedThreads.length; i++) {
			delete this.threads[finishedThreads[i]]
		}
	}
}

